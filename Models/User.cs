﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BingoWeb.Models
{
    public class User
    {
        [Key]
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
